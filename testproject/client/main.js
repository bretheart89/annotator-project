import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import jquery from 'jquery';
import annotator from 'annotator';
import './main.html';

// if ( Meteor.is_client ) {
    Meteor.startup(function () {
        // $(function(){
        	//console.log("Test project");
        	// var ann = new Annotator(document.body);
        	// $('body').annotator();
        	if (typeof annotator === 'undefined') {

		        console.log("Oops! it looks like you haven't built Annotator. " +
		              "Either download a tagged release from GitHub, or build the " +
		              "package by running `make`");
		      
		      } else {
	        	var elem = document.querySelector('article');
	        	console.log(elem);
			      var app = new annotator.App()
				      .include(annotator.ui.main, {element: elem})
				      .include(annotator.ui.filter.standalone)
				      .include(annotator.storage.debug)
				      .start()

		        // console.log(app);
		      }
        	// $("p").css("color", "red");
        // }) 
    });
// }

Template.hello.onCreated(function helloOnCreated() {
  // counter starts at 0
  this.counter = new ReactiveVar(0);
});

Template.hello.helpers({
  counter() {
    return Template.instance().counter.get();
  },
}); 

Template.hello.events({
  'click button'(event, instance) {
    // increment the counter when button is clicked
    instance.counter.set(instance.counter.get() + 1);
  },
}); 
